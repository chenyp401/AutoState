# 状态机自动生成工具

#### 介绍
图形化配置状态机并自动生成相应代码<br/>支持子状态分量的配置

#### 软件架构
winform


#### 安装教程

1.  在可执行程序DiagramDemo.exe同级目录新建文件夹DiagromShapes
2.  复制BasicsShape.dll到目录DiagromShapes下
3.  复制FastAutomationFrame.Diagram.dll到可执行程序DiagramDemo.exe同级目录下
4.  双击DiagramDemo.exe运行程序

#### 使用说明

1.  将左侧状态方框拖入绘图区
2.  点击状态方框，填入左侧Attribute下的Name属性，该属性即为状态名
3.  点击状态方框后四周出现箭头，点击箭头可以连接至其他状态，表示该状态可以转换至其他状态
![avatar](/img/mian.jpg)
4.  双击状态方框或选中状态方框后按下+键可以创造一个子状态域，子状态域的操作和主状态相同
5.  按下Esc键可以退出当前子状态域返回父状态域
6.  点击**代码生成**选择保存目录后可以保存生成的代码 ***fsm.c*** 和 ***fsm.h***

#### 代码架构
1.  对于状态名一律使用用户填入Name的大写，函数使用Name的小写
1.  对于子状态，状态名将会带上父状态的状态名，比如**OPEN**状态下的**LOW**状态，系统自动编写名字为**OPEN_LOW**，在该状态下还有**SLEEP**状态，则编写名字为**OPEN_LOW_SLEEP**
1.  代码分为主状态和子状态，子状态是否有自己的子状态都归类于子状态
1.  ```void fsm_loop();```为轮询函数
1.  ```***_run()```以run结尾的函数都运行在fsm_loop()中，表示当前状态下所需要进行的操作，对于子状态，只有在父状态运行时才会运行对应子状态
1.  ``` ***_to_***()```中间带有to的函数表示状态转换所进行的操作，状态转化只会在同级状态中进行
1.  ```
    unsigned char get_state_now();
    void set_state_now(unsigned char state);
    ```
    表示主函数的设置和获取
1.  ```
    void set_sub_state(unsigned char state,unsigned char substate);
    unsigned char get_sub_state(unsigned char state);
    ```
    表示子状态的设置和获取，其中state表示父状态的状态名
1.  所有的```**_to_**```函数都直接在set()函数中运行，注意是否阻塞

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

