#ifndef _FSM_H_
#define _FSM_H_

enum{
    STATE_ON  = 0,
    STATE_OFF = 1,
    STATE_RUN = 2,
};
enum{
    STATE_ON_ON  = 0,
    STATE_ON_OFF = 1,

    STATE_OFF_ON  = 2,
    STATE_OFF_OFF = 3,

    STATE_OFF_ON_ON  = 4,
    STATE_OFF_ON_OFF = 5,
};
void set_sub_state(unsigned char state,unsigned char substate);
unsigned char get_sub_state(unsigned char state);

void fsm_loop();
unsigned char get_state_now();
void set_state_now(unsigned char state);
void state_on_to_state_off();
void state_on_to_state_run();
void state_off_to_state_run();
void state_off_to_state_on();

void state_on_to_state_off();
void state_off_on_run();

void state_on_run();
void state_off_run();
void state_run_run();
#endif // !_FSM_H_