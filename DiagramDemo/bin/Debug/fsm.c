#include "fsm.h"
unsigned char stateNow;
unsigned char state_on_sub_now;
unsigned char state_off_sub_now;
unsigned char state_off_on_sub_now;
unsigned char get_state_now()
{
    return stateNow;
}
void state_on_run()
{
}
void state_off_run()
{
}
void state_run_run()
{
}
// sub run
void state_off_on_run()
{

}
// change state
void state_on_to_state_off()
{
}
void state_on_to_state_run()
{
}
void state_off_to_state_run()
{
}
void state_off_to_state_on()
{
}
// substate change
void state_on_to_state_off()
{

}
void set_state_now(unsigned char state)
{
    switch (stateNow)
    {
        case STATE_ON:
            switch (state)
            {
                case STATE_OFF:
                    state_on_to_state_off();
                    break;
                case STATE_RUN:
                    state_on_to_state_run();
                    break;
                default:
                    break;
            }
            break;
        case STATE_OFF:
            switch (state)
            {
                case STATE_ON:
                    state_off_to_state_on();
                    break;
                case STATE_RUN:
                    state_on_to_state_run();
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    stateNow = state;
}
void fsm_loop()
{
    switch (stateNow)
    {
        case STATE_ON:
            state_on_run();
            break;
        case STATE_OFF:
            state_off_run();
            break;
        case STATE_RUN:
            state_run_run();
            break;
        default:
            break;
    }
    switch (state_on_sub_now)
    {
    case STATE_OFF_ON:
        state_off_on_run();
        break;
    
    default:
        break;
    }
}

void set_sub_state(unsigned char state, unsigned char substate)
{
    switch (state)
    {
        case STATE_ON:
            switch (substate)
            {
                case STATE_ON_OFF:
                    switch (state_on_sub_now)
                    {
                        case STATE_ON_ON:
                            state_on_off_to_state_on_on();
                            break;
                    }
                    break;
                case STATE_ON_ON:
                    switch (state_on_sub_now)
                    {
                        case STATE_ON_OFF:
                            state_on_on_to_state_on_off();
                            break;
                    }
                    break;
            }
            state_on_sub_now = substate;
            break;
        case STATE_OFF:
            switch (substate)
            {
                case STATE_OFF_ON:
                    switch (state_off_sub_now)
                    {
                        case STATE_OFF_OFF:
                            state_off_on_to_state_off_off();
                            break;
                    }
                    break;
                case STATE_OFF_OFF:
                    switch (state_off_sub_now)
                    {
                        case STATE_OFF_ON:
                            state_off_off_to_state_off_on();
                            break;
                    }
                    break;
            }
            state_off_sub_now = substate;
            break;
        case STATE_OFF_ON:
            switch (substate)
            {
                case STATE_OFF_ON_ON:
                    switch (state_off_sub_now)
                    {
                        case STATE_OFF_ON_OFF:
                            state_off_on_on_to_state_on_off();
                            break;
                    }
                    break;
                case STATE_OFF_ON_OFF:
                    switch (state_off_sub_now)
                    {
                        case STATE_OFF_ON_ON:
                            state_off_on_off_to_state_off_on_on();
                            break;
                    }
                    break;
            }
            state_off_sub_now = substate;
            break;
    }
}
unsigned char get_sub_state(unsigned char state)
{
    switch (state)
    {
        case STATE_ON:
            return state_on_sub_now;
            break;
        case STATE_OFF:
            return state_off_sub_now;
            break;
        case STATE_OFF_ON:
            return state_off_on_sub_now;
            break;
    }
}
